const express = require('express');
const app = express();
const port = 3000;
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors({
    origin: '*'
}));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

let customers = [];


// read records
function fetchJsonData() {
    fs.readFile('server/records.json', 'utf8', (err, data) => {
        if (err) {
          console.error(err);
          return;
        }
        customers = JSON.parse(data);
        return data;
      });
}

// initialize data
fetchJsonData();

// generate timestamp
function makeTimeStamp() {
    return new Date(Date.now())
}

// find existing record
function findRecord(id) {
    try {
        let record = customers.find(obj => obj.id === id);
        return record
    } catch (error) {
        console.log(error);
        return;
    }
}

// update existing customer
function updateCustomer(formData, id) {
    // fetch record
    let current = findRecord(id);
    // set profile data
    current.profile = {
        firstName: formData.firstName,
        lastName: formData.lastName,
        email: formData.email
    }
    // add timestamp for update
    current.updatedAt = makeTimeStamp();
    return;
}

// add new customer
function addCustomer(customer) {
    // create user id 
    function generateId() {
        const rand = () => Math.random().toString(36).substr(2);
        const token = () => rand() + rand();
        return token();
    }

    // build new customer obj
    let newCustomer = {
        id: generateId(),
        profile: {
            firstName: customer.firstName || '',
            lastName: customer.lastName || '',
            email: customer.email || ''
        },
        createdAt: makeTimeStamp()
    }

    // add to list
    customers.push(newCustomer)
    return;
}

// delete customer
function deleteCustomer(id) {
    const itemIndex = customers.findIndex((obj) => obj.id === id);
    if (itemIndex > -1) {
        customers.splice(itemIndex, 1);
    }
    return;
}

app.get('/items', (req, res) => {
    res.send(customers)
})

app.get('/items/:id', (req, res) => {
    let item = findRecord(req.params.id);
    res.send(item);
})

app.post('/items', (req, res, {}) => {
    addCustomer(req.body);
    res.send(customers)
})

app.patch('/items/:id', (req, res)=> {
    updateCustomer(req.body, req.params.id)
    res.send(customers)
})

app.delete('/items/:id', (req, res)=> {
    deleteCustomer(req.params.id);
    res.send(customers)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});