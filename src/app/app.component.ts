import { Component } from '@angular/core';
import { DataService } from './services/data.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    DataService
  ]
})
export class AppComponent {
  title = 'Dynatron';
  customers: any = [];
  selectedCustomer: any = undefined;
  modalContext: string = '';
  

  constructor(
    private dataService : DataService,
    ) { } 

  getData() { 
    this.dataService.readData().subscribe(data => this.customers = data);
  }

  addCustomer(customerData: object) {
    this.dataService.createItem(customerData).subscribe(data => this.customers = data);
  }

  updateCustomer(customerData: object) {
    this.dataService.updateItem(this.selectedCustomer.id, customerData).subscribe(data => this.customers = data);
  }

  deleteCustomer() {
    this.dataService.removeItem(this.selectedCustomer.id).subscribe(data => this.customers = data);
  }
  
  ngOnInit() {     
    this.getData()    
  }

  onClickSubmit(form: NgForm ): void {
    let formValues = form.value;
    console.warn('Updated Values: ', formValues);
    // this.updateCustomer(formValues);
    switch (true) {
      case this.modalContext === 'update':
        this.updateCustomer(formValues);
        break;
      case this.modalContext === 'add':
        this.addCustomer(formValues);
        break;
      default:
        break;
    }
  }

  public setContext(customer: object, type: string) {
    this.selectedCustomer = customer;
    this.modalContext = type;
    sessionStorage.setItem("customer", JSON.stringify(customer))
  }
}
