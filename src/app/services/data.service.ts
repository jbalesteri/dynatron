import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class DataService {
    private _jsonURL = 'http://localhost:3000';
    constructor(private http: HttpClient) {}

    public createItem(item: object): Observable<any> {
        let data = this.http.post(`${this._jsonURL}/items`, item, {});
        return data
    }

    public fetchData(): Observable<any> {
        let data = this.http.get(`${this._jsonURL}/items`);
        return data;
    }

    public readData(): Observable<any> {
        let data = this.http.get(`${this._jsonURL}/items`);
        return data;
    }

    public updateItem(id: string, item: any): Observable<any> {
        let data = this.http.patch(`${this._jsonURL}/items/${id}`, item, {});
        return data;
    }

    public removeItem(id: any): Observable<any> {

        let data = this.http.delete(`${this._jsonURL}/items/${id}`, {})
        return data;
    }
}