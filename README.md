# Cutomer List (Angular/Express)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.1.6.

## Development server

Run `npm run dev` for a dev server. Navigate to `http://localhost:4200/` for front end application and server will run on `http://localhost:3000/`

The application will automatically reload if you change any of the source files.

## Software Engineer Coding Challenge

The purpose of this challenge is to highlight the engineer's skills. Therefore, feel free to show-off
and speak to your solution for how and why you chose to do things the way you choose to do
them.

### Assessment:
1. Using Angular 13 or above (via VS Code) and NET Core (via Visual Studio), create a
simple CRUD application for "Customer" (First Name, Last Name, Email, Created
Date/Time, Last Updated Date/Time) communicating to service.
2. Allow for Updates from the list of customers while requiring adds via form
3. Ensure the application uses session storage to highlight the last customer selected.
4. Ensure application has at least 100 records.
5. Provide repository link for Dynatron Tech Team to download code for review
6. Be prepared to speak to your solution on scheduled Tech Call.

While listing every possible factor that could impact an application would both be two exhaustive
for this assessment and also provide some answers potentially to things you should implement
or at least speak to. Therefore, if there are factors you feel compelled to implement to showcase
your talent, feel free. If there are other things you choose not to implement because it would be
too time consuming, feel free to speak to it so we are able to understand you do have the
knowledge but it would be impractical to implement for this assessment. That decision is left to
you on how much and how little to include.